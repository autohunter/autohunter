#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Implement some basic behaviours.  Partly as example, but they
# are useful on their own in a real-world application, too.

import logging
import maputils
import random
import souls

def alreadyMovingTowards (hunter, targets, sight = None):
  """Check whether the hunter already moves towards one of the targets.

  This is a utility routine that may come in handy to decide whether or
  not some "collector" behaviour needs changes to the current movement.
  It checks whether one of the 'targets' is already on the hunter's current
  movement path and reached in maximum time 'sight'.

  Note that it doesn't consider path linearisations but only finds a target
  if it is an explicit waypoint of the hunter's current movement path."""

  if isinstance (targets, tuple):
    targets = [targets]

  movement = hunter.getMovement ()
  if movement is not None:
    time = 0
    pos = hunter.getPosition ()
    for m in movement:
      time += maputils.distLInf (pos, m)
      if sight is not None and time > sight:
        return False

      if m in targets:
        return True

      pos = m

  return False

def findClosest (position, targets, sight, m):
  """From the given targets, find the closest one.

  Look for the closest entry of the 'targets' list (by L-infinity
  distance for simplicity).  But after sorting by distance, pick
  one that really has a *graph* distance at most 'sight'.  Returns None
  if no such entry can be found."""

  def distanceCmp (a, b):
    da = maputils.distLInf (position, a)
    db = maputils.distLInf (position, b)
    return da - db
  sortedTargets = sorted (targets, distanceCmp)

  for coord in sortedTargets:
    if m.getDistance (position, coord) <= sight:
      return coord

  return None

################################################################################
# HeartCollector

class HeartCollector (souls.Behaviour):
  """Implement a behaviour to collect hearts.

  This implements a behaviour to look for hearts in some area
  around the current position, and to try to collect them."""

  def __init__ (self, sight):
    """Construct the behaviour.  Looking 'sight' cells away.

    Note that the L-infinity distance is used for performance reasons
    rather than the graph distance.  All hearts within this radius
    are then sorted again by L-infinity.  If the found heart has a graph
    distance further away than the sight, don't move there.  This is
    a bit heuristic and may not always be optimal, but in practice it should
    not be too bad."""

    self._sight = sight

  def play (self, hunter, control):
    """Implement the play() logic."""

    # If we move already to a heart, just continue.
    if alreadyMovingTowards (hunter, control.map.hearts, self._sight):
      logging.info ("HeartCollector %s: Already moving towards heart.",
                    hunter.toString (False))
      hunter.moveWait ()
      return

    # Look for hearts in sight.
    position = hunter.getPosition ()
    inSight = filter (maputils.getLInfFilter (position, self._sight),
                      control.map.hearts)

    # Try to find the best heart, if any.
    target = findClosest (position, inSight, self._sight, control.map)
    if target is not None:
      logging.info ("HeartCollector %s: Moving to heart at %s.",
                    hunter.toString (False), str (target))
      hunter.movePath (target)

################################################################################
# Gatherer

class Gatherer (souls.Behaviour):
  """Implement a behaviour to gather coins.

  This behaviour looks for coins either around the current hunter
  or around a defined "centre" point.  It tries to move to the closest
  coin or coins (forming a path with some defined maximal number of
  coins on it).  The coin value is ignored for simplicity."""

  def __init__ (self, sight, maxPathLen = 1, centre = None):
    """Construct the behaviour.  Looking 'sight' cells away.

    If 'centre' is given, always look 'sight' cells around the centre
    for coins rather than around the current position.  This can be used
    to fix the hunter to some area and prevent it moving step-by-step
    away.  If no centre is given, it always looks around the current
    position of the hunter.

    'maxPathLen' can be used to define how many coins are tried to be
    collected in a single path."""

    self._sight = sight
    self._centre = centre
    self._maxPathLen = maxPathLen

    self._forceWait = True

  def setForcedWait (self, val):
    """Set the 'forceWait' parameter.

    This parameter (default True) determines whether the play() routine
    forces a 'wait' move onto a hunter when no coins are in sight.  Setting
    it to False can be used to stack other, less important but still
    useful behaviours below it."""

    self._forceWait = val

  def play (self, hunter, control):
    """Implement the play() logic."""

    # If already moving towards a coin, just continue.
    loot = control.map.loot.keys ()
    if alreadyMovingTowards (hunter, loot, self._sight):
      logging.info ("Gatherer %s: Already moving towards coin.",
                    hunter.toString (False))
      hunter.moveWait ()
      return

    # Look for loot in sight.
    if self._centre is None:
      centre = hunter.getPosition ()
    else:
      centre = self._centre
    inSight = set (filter (maputils.getLInfFilter (centre, self._sight), loot))

    # Try to build up a path connecting the loot.
    p = []
    pos = hunter.getPosition ()
    while len (p) < self._maxPathLen:
      target = findClosest (pos, inSight, self._sight, control.map)
      if target is None:
        break
      inSight.remove (target)
      p.append (target)
      pos = target

    # If we have one, go for it.
    if len (p) > 0:
      totalCoins = 0
      for target in p:
        totalCoins += control.map.loot[target]
      logging.info ("Gatherer %s: Moving towards %.8f coins in %d steps.",
                    hunter.toString (False), totalCoins, len (p))
      hunter.movePath (p)
      return

    # Move towards centre if we have nothing else to do.
    if self._centre is not None:
      if alreadyMovingTowards (hunter, self._centre):
        logging.info ("Gatherer %s: Already moving to centre.",
                      hunter.toString (False))
        hunter.moveWait ()
        return

      if self._centre == hunter.getPosition ():
        if self._forceWait:
          logging.info ("Gatherer %s: Already at centre, waiting.",
                        hunter.toString (False))
          hunter.moveWait ()
        else:
          logging.info ("Gatherer %s: Already at centre, not forcing wait.",
                        hunter.toString (False))
        return

      logging.info ("Gatherer %s: Moving to centre at %s.",
                    hunter.toString (False), str (self._centre))
      hunter.movePath (self._centre)
      return

################################################################################
# CashIn

class CashIn (souls.Behaviour):
  """Go to cash in coins over a certain treshold.

  Whenever the loot collected by the controlled hunter is above some
  treshold, go to cash it in in its colour's spawn area.  Afterwards,
  it can either move back to a fixed location, to the original position
  or do nothing."""

  def __init__ (self, treshold, moveBack = False):
    """Construct the behaviour with the given parameters.

    We move back to the spawn once the hunter's loot is at least
    'treshold'.  'moveBack' determines how we move back afterwards:
    A coordinate tuple makes it move back to this position.  True means
    to move back to the position the hunter started from, and
    False means no moving back at all."""

    self._treshold = treshold
    self._moveBack = moveBack

  def play (self, hunter, control):
    """Implement the play() logic."""

    # If we are below the loot treshold, nothing to do.
    if hunter.getLoot () < self._treshold:
      return

    # Find out spawn positions.
    spawn = control.map.getSpawnArea (hunter.getColour ())

    # If already moving towards it, we're just done.
    if alreadyMovingTowards (hunter, spawn):
      logging.info ("CashIn %s: Already moving towards spawn.",
                    hunter.toString (False))
      hunter.moveWait ()
      return

    # Move to one random spawn position.  Take care of the moveBack
    # configuration parameter.
    path = [random.choice (spawn)]
    if isinstance (self._moveBack, tuple):
      path.append (self._moveBack)
    elif self._moveBack:
      path.append (hunter.getPosition ())

    logging.info ("CashIn %s: We have %.8f HUC, cashing in.",
                  hunter.toString (False), hunter.getLoot ())
    hunter.movePath (path)

################################################################################
# BasicGatherer

class BasicGatherer (souls.ChooseByColour, souls.StackedBehaviours, souls.Soul):
  """Soul implementation that combines the Gatherer and CashIn behaviours.

  This implements a "complete" (but very basic) gathering bot.  It
  looks for coins around some defined centre, picks them up
  and cashes in once a certain treshold is reached."""

  def __init__ (self, colour, centre, sight, treshold,
                maxPathLen = 1, recreate = True):
    """Initialise with the given configuration info.

    For the meaning of the individual options, see in the
    Gatherer and ChooseByColour classes."""

    souls.Soul.__init__ (self)
    souls.ChooseByColour.__init__ (self, colour, recreate)
    souls.StackedBehaviours.__init__ (self)

    self.addLowPrio (Gatherer (sight, maxPathLen, centre))
    self.addHighPrio (CashIn (treshold, centre))
