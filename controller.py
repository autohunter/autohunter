#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The "main loop" and encapsulation of the whole world
# state, including all players, hunters, bots -- everything.

import gamestate
import logging
import maputils
import os.path
import pickle
import player
import random
import string

GENERATED_NAME_LENGTH = 10
NAME_CHARACTERS = string.ascii_lowercase + string.digits
RESCUE_SPAWN_STAY = 20

################################################################################
# NameSelector

class NameSelector (object):
  """Select (and generate) player names.

  This class takes care of selecting (among all names in the daemon's wallet)
  player names that should be controlled by some criterion.  It is also
  capable of generating new names matching this criterion.

  The criterion used is by prefixing names with some fixed string."""

  def __init__ (self, prefix = ""):
    """Construct the selector using a given prefix."""

    self._prefix = prefix;

  def check (self, name):
    """Check whether the given name conforms to the criterion."""

    n = len (self._prefix)
    namePrefix = name[:n]

    return namePrefix == self._prefix

  def generate (self, state = None):
    """Generate a name matching our criterion.

    If the game state is given, only generate a name that doesn't
    yet exist on the map."""

    n = GENERATED_NAME_LENGTH - len (self._prefix)
    if n <= 0:
      raise RuntimeError ("name prefix '%s' is too long for name length %d",
                          self._prefix, GENERATED_NAME_LENGTH)

    while True:
      res = self._prefix
      while len (res) < GENERATED_NAME_LENGTH:
        res += random.choice (NAME_CHARACTERS)
      assert self.check (res)

      if (state is None) or (not state.nameExists (res)):
        return res

################################################################################
# Strategy

class Strategy (object):
  """A particular playing strategy.

  This is an abstract base class for a 'playing strategy'.  It
  defines what to do with a Controller object when the game state
  is updated.  This includes the responsibility to reserve/activate
  players as necessary, and to move hunters around."""

  def play (self, control):
    """Perform a step on the passed Controller object.

    This is the abstract method that needs to be implemented
    by subclasses.  The core method does nothing except
    printing a warning to the user.

    The controller takes care to send all queued moves
    of 'mine' hunters after this routine has finished.

    If the method returns False, the Controller's main loop will be
    terminated after sending the moves."""

    logging.warning ("Base strategy called, doing nothing.")
    return True

################################################################################
# BaseStrategy

class BaseStrategy (Strategy):
  """Basic strategy that is applied by the Controller as fall-back.

  This is a very basic Strategy implementation, which is always
  employed by the Controller after the actual Strategy.  It just
  moves hunters out of the spawn area when they are threatened
  to be killed there."""

  def play (self, control):
    """Play the strategy, moving hunters out of spawn."""

    for h in control.hunters["mine"]:

      # Ignore hunters with pending moves.
      if h.status != "active":
        assert h.status == "pending"
        continue

      # Do not interfere with existing moves!
      if h.isMoving () or h.hasMove ():
        continue

      # See if we are in danger.  Note that even if a hunter's
      # 'stay_in_spawn_area' is non-zero, we still have to check
      # if the coord actually *is* in spawn since it will only
      # be reset after moving out with one block of delay.
      coord = h.getPosition ()
      stay = h.state["stay_in_spawn_area"]
      if control.map.isSpawnArea (coord) and stay > RESCUE_SPAWN_STAY:
        logging.info ("%s is in spawn for %d blocks, moving out.",
                      h.toString (False), stay)

        def moveCoordOut (c):
          if c == 0:
            return 1
          if c == maputils.MAP_SIZE - 1:
            return maputils.MAP_SIZE - 2
          return c

        target = tuple (map (moveCoordOut, coord))
        h.movePath (target)

    return True

################################################################################
# Controller

class Controller (object):
  """The main loop and 'whole world'.

  The Controller object manages the whole 'world', including
  the game state, map util, players and all that.  It also
  includes the main loop.

  Public member variables:
    state: Game state object.
    map: The Map object corresponding to the current state.
    players: Dictionary of Player object lists on the map, which is indexed
             by certain keywords to select among available lists
             of players.  See below.
    hunters: Same as 'players', but containing a list of all hunters
             of these players instead.

  Available player lists:
    "all": All players on the map.
    "mine": Players in the wallet matching also the name selector.
    "friendly": Players in the wallet not matching name selector.
    "foreign": Players not in the wallet."""

  def __init__ (self, ns, rpc):
    """Construct the object, using the given RPC interface.

    A NameSelector instance must be provided."""

    self._ns = ns
    self._rpc = rpc
    self.state = gamestate.GameState (self._rpc)
    self.map = maputils.Map (self._rpc, self.state)

    self._baseStrategy = BaseStrategy ()
    self._persist = None

    self._players = {}
    self._update ()

  def persist (self, name):
    """Enable persistence to the given file name.

    This routine enables persistence to the given file name.  If the
    file does not exist, it just remembers this information and ensures
    that both the Controller and the Strategy objects will be written
    accordingly down in the main loop.

    If the file does exist, the stored state is loaded and the main loop
    immediately started at the position where it left off.

    Note that in the latter case the 'self' object is not restored
    (since a new object is loaded and used instead), so if the main loop
    terminates and this function returns, 'self' is still the
    newly created Controller it was in the beginning.  This function
    returns False if this is the case."""

    if not os.path.isfile (name):
      logging.info ("Persisting to %s.", name);
      self._persist = name
      return True

    logging.info ("Loading persisted state.");
    with open (name, "r") as f:
      # Make sure to use a single pickle call, since we want to
      # ensure pointer consistency between both objects saved.
      (control, strategy) = pickle.load (f)

    control.state.updateNow ()
    control._update ()
    control.loop (strategy)
    return False

  def loop (self, strategy = Strategy (), immediate = True):
    """Start the main loop.

    It uses the passed in Strategy object to decide about game
    operations after each state update.  The default value
    is just a 'base strategy' that does nothing.  This is
    probably not what you want.

    If 'immediate' is set, the first strategy update will be played
    immediately without waiting for a change in the game state.
    Otherwise, the wait comes first to ensure that we really have
    an up-to-date game state."""

    logging.info ("Starting controller main loop.")
    wait = not immediate
    while True:
      if wait:
        if self._persist is not None:
          with open (self._persist, "w") as f:
            pickle.dump ((self, strategy), f)

        self.state.waitForChange ()
        self._update ()
      wait = True

      ok = strategy.play (self)
      self._baseStrategy.play (self)
      self._sendMoves ()

      # TODO: Maybe implement some logic to keep on with
      # the main loop (even if the strategy is finished)
      # until all pending player registrations are done
      # and the players in safety.

      if not ok:
        break

  def playersInReserve (self):
    """Return the number of players in reserve.

    Players in reserve are already name_new'ed and can be launched
    (activated) immediately."""

    return len (self._reservePlayers)

  def playersInRegistration (self):
    """Return the number of players in 'registering' status.

    Return how many players are not yet ready for the reserve pool,
    but in 'registering' status.  This is useful to decide whether
    we need more or not."""

    return self._registeringPlayers

  def playersInActivation (self, colour = "all"):
    """Return the number of players in 'activating' status.

    If the colour attribute is set to "array", it returns an array
    indexed by colour.  If it is set to an integer, return only the
    number of players being activated to the given colour.  If no value
    is set or the value is "all", return the total number of players
    in activation state."""

    if colour == "all":
      return sum (self._activatingPlayers)
    if colour == "array":
      return self._activatingPlayers

    return self._activatingPlayers[colour]

  def registerPlayers (self, n = 1):
    """Start registration of n new players.

    Start the registration of n new players.  This issues appropriate
    name_new commands for names generated according to the NameSelector,
    so that we have more reserve players once the transactions confirm."""

    added = 0
    while added < n:
      name = self._ns.generate (self.state)
      if name not in self._players:
        p = player.Player (name, self._rpc, self.map, self.state)
        self._players[name] = p
        p.register ()
        added += 1

  def activatePlayer (self, colour):
    """Activate a new player to the given colour.

    If we have available players in reserve, activate one of them
    to the given colour and return it.  Returns None if there are
    no players in reserve."""

    if self.playersInReserve () == 0:
      return None

    p = self._reservePlayers.pop ()
    p.activate (colour)
    return p

  def getPlayer (self, name):
    """Get a player by name."""

    if name not in self._players:
      return None
    return self._players[name]

  def getHunter (self, name, ind):
    """Get a hunter by name and index."""

    p = self.getPlayer (name)
    if p is None:
      return p

    if ind not in p.hunters:
      return None
    return p.hunters[ind]

  def getCrown (self):
    """Return the crown position or holder.

    This returns either the crown position as a tuple if
    the crown is on the ground, or the holder as Hunter
    object if it is not."""

    crown = self.state.state["crown"]
    if "holderName" in crown:
      return self.getHunter (crown["holderName"], crown["holderIndex"])

    return maputils.coordFromJson (crown)

  def _update (self):
    """Update internally for the new game state."""

    self.map.update (self.state)
    for (i, p) in self._players.iteritems ():
      p.update (self.state)

    # Add newly constructed players for every name that exists
    # in the game state but not yet the player list.
    added = 0
    for key in self.state.state["players"]:
      if not key in self._players:
        p = player.Player (key, self._rpc, self.map, self.state)
        added += 1
        self._players[key] = p
    if added > 0:
      logging.info ("Added %d new players.", added)

    # Remove players with 'available' status.  They are not
    # really present and also don't carry registration information,
    # so probably just have been killed and can be removed.
    removed = 0
    newPlayers = {}
    for (k, p) in self._players.iteritems ():
      if p.status == "available":
        removed += 1
      else:
        newPlayers[k] = p
    assert removed + len (newPlayers) == len (self._players)
    if removed > 0:
      logging.info ("Removed %d 'available' players.", removed)
    self._players = newPlayers

    logging.info ("Updated world state, we have %d players.",
                  len (self._players))
    self._updatePlayerLists ()
    self._findReservePlayers ()

  def _updatePlayerLists (self):
    """Set the 'players' and 'hunters' member variables."""

    self.players = {"all": [], "mine": [], "friendly": [], "foreign": []}
    for key in self.state.state["players"]:
      assert key in self._players
      p = self._players[key]
      assert p.status in ["pending", "active", "foreign"]

      self.players["all"].append (p)
      if p.status == "foreign":
        self.players["foreign"].append (p)
      elif self._ns.check (p.name):
        self.players["mine"].append (p)
      else:
        self.players["friendly"].append (p)

    self.hunters = {}
    for (k, lst) in self.players.iteritems ():
      self.hunters[k] = []
      for p in lst:
        for (i, h) in p.hunters.iteritems ():
          self.hunters[k].append (h)

    def printStats (name, arr):
      stat = "%s statistics:" % name
      for (k, lst) in arr.iteritems ():
        stat += "\n  %s: %d" % (k, len (lst))
      logging.info (stat)

    printStats ("Player", self.players)
    printStats ("Hunter", self.hunters)

  def _findReservePlayers (self):
    """Build up the _reservePlayers member variable.

    Look through the list of 'registered' players matching the
    name criterion and add them to the list of available 'reserve
    players'."""

    self._reservePlayers = []
    self._registeringPlayers = 0
    self._activatingPlayers = [0, 0, 0, 0]
    for (k, p) in self._players.iteritems ():
      if self._ns.check (p.name):
        if p.status == "registered":
          assert not self.state.nameExists (p.name)
          self._reservePlayers.append (p)
        elif p.status == "registering":
          assert not self.state.nameExists (p.name)
          self._registeringPlayers += 1
        elif p.status == "activating":
          assert not self.state.nameExists (p.name)
          self._activatingPlayers[p._firstupdateColour] += 1

    logging.info ("We have %d reserve players, %d in registration"
                  " and %d being activated.",
                  len (self._reservePlayers), self._registeringPlayers,
                  sum (self._activatingPlayers))

  def _sendMoves (self):
    """Send all queued moves of 'mine' players."""

    for p in self.players["mine"]:
      assert p.status in ["pending", "active"]
      if p.status == "active":
        p.sendMoves ()
