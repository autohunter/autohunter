#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Encapsulate a game state and wait for updates when new
# blocks arrive.  The "game state" is not only the state
# as returned by game_getstate, but also does a name_list call
# to retrieve status about players in the wallet.

import logging

class GameState(object):
  """Encapsulate a game state.

  Public member variables:
    state: The raw 'game_getstate' object.
    names: List of names in the wallet (like 'name_list') that are
           active, i. e., neither transferred nor killed."""

  def __init__ (self, rpc):
    """Construct the object, using the given RPC interface."""

    self._rpc = rpc
    self.updateNow ();

  def updateNow (self):
    """Update immediately with a game_getstate call."""

    state = self._rpc.call ("game_getstate", [])
    self._update (state)

  def waitForChange (self):
    """Wait until a new block arrives, then update the state."""

    lastHash = self.state["hashBlock"]
    state = self._rpc.call ("game_waitforchange", [lastHash])
    self._update (state)

  def nameExists (self, name):
    """Check if the name exists as an active player's name.

    It doesn't matter who owns the player."""

    return name in self.state["players"]

  def timeSinceDisaster (self):
    """Return the time since the last disaster."""

    height = self.state["height"]
    disasterHeight = self.state["disasterHeight"]

    assert height >= disasterHeight
    return height - disasterHeight

  def _update (self, state):
    """Set to the given game state and call name_list to update names."""

    self.state = state
    logging.info ("NEW GAME STATE @%d", self.state["height"])

    # Construct a set of active player names in the wallet.  Filter out
    # dead and transferred ones.
    names = self._rpc.call ("name_list", [])
    self.names = []
    for entry in names:
      if (not "dead" in entry) and (not "transferred" in entry):
        self.names.append (entry["name"])
