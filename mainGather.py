#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The main program (including configuration) for simple gathering bots.
# It serves as an example for how to use the framework.

import behaviours
import controller
import logging
import namerpc
import souls
import sys

logging.basicConfig (level=logging.INFO)

# Define RPC connection options.
options = dict (rpcuser="huntercoind", rpcpassword="password", rpcport=18399)
rpc = namerpc.CoinRpc ("client", options)
logging.info ("Huntercoin RPC client created.")

if rpc.is_locked ():
  logging.critical ("Wallet is locked.")
  sys.exit ()

# Define name prefix to be used for created players.
ns = controller.NameSelector ("domob-")
controller = controller.Controller (ns, rpc)

strat = souls.SoulMaster ()

yellow = 0
red = 1
green = 2
blue = 3

# Add individual gatherers.  The configuration options are:
#   1) Colour of the hunter.
#   2) Centre of gathering spot.
#   3) Radius (diagonal, too) for the spot around the centre.
#   4) Loot to collect before cashing in.
#   5) Maximum number of coins connected by a single path.
#   6) Whether to recreate the hunter when killed.

strat.addSoul (behaviours.BasicGatherer (yellow, (133, 63), 4, 10, 3, True))
strat.addSoul (behaviours.BasicGatherer (red, (396, 11), 9, 10, 3, True))
strat.addSoul (behaviours.BasicGatherer (green, (281, 406), 3, 10, 3, True))
strat.addSoul (behaviours.BasicGatherer (blue, (75, 432), 3, 10, 3, True))

if controller.persist ("gather.state"):
  controller.loop (strat)
