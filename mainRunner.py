#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Main program for the disaster runners.

import behaviours
import controller
import logging
import namerpc
import runner
import sys

logging.basicConfig (level=logging.INFO)

# Define RPC connection options.
options = dict (rpcuser="huntercoind", rpcpassword="password", rpcport=18399)
rpc = namerpc.CoinRpc ("client", options)
logging.info ("Huntercoin RPC client created.")

if rpc.is_locked ():
  logging.critical ("Wallet is locked.")
  sys.exit ()

# Define name prefix to be used for created players.
ns = controller.NameSelector ("domob-")
controller = controller.Controller (ns, rpc)

yellow = 0
red = 1
green = 2
blue = 3

# Define the gathering behaviours.  Construct one of each colour,
# so that we have chances no matter where the crown pops up.

maxPathLen = 5
treshold = 10

gatherers = []
def addGatherer (colour, centre, sight):
  gather = behaviours.Gatherer (sight, maxPathLen, centre)
  cashIn = behaviours.CashIn (treshold, centre)
  gatherers.append ({"colour": colour, "gather": gather, "cashIn": cashIn})

# Add the gatherers.  The configuration options are:
#   1) Colour of the hunter.  Ideally have (at least) one of each colour.  This
#      gives you the most flexibility in hunting for the crown.
#   2) Centre of gathering spot.
#   3) Radius (diagonal, too) for the spot around the centre.
# 
# Note that one of these (the one closest to the crown) will be dispatched
# for the crown instead.

addGatherer (yellow, (69, 132), 4)
addGatherer (red, (396, 11), 9)
addGatherer (green, (369, 439), 3)
addGatherer (blue, (70, 310), 3)

# Start the main loop.
strat = runner.DisasterRunner (gatherers)
if controller.persist ("runner.state"):
  controller.loop (strat)
