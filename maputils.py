#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Some routines with respect to interpreting the map, finding paths
# and all that.

MAP_SIZE = 502
SPAWN_LEN = 15

def unpackWaypoints (wp):
  """Convert a waypoints array in the daemon format to a list of tuples."""

  if len (wp) % 2 != 0:
    raise RuntimeError ("waypoints array should have odd length")

  res = []
  for i in range (0, len (wp), 2):
    res.append (tuple (wp[i : i + 2]))

  return res

def packWaypoints (pts):
  """Convert a tuple array to a flat list of waypoints."""

  res = []
  for coord in pts:
    res.extend (coord)

  return res

def distLInf (a, b):
  """Calculate the L-infinity distance between two coordinates."""

  return max (abs (a[0] - b[0]), abs (a[1] - b[1]))

def pathLen (start, pts):
  """Calculate the path length for a list of tuples."""

  res = 0
  for coord in pts:
    res += distLInf (start, coord)
    start = coord
  
  return res

def coordFromJson (obj):
  """Convert a JSON object with 'x' and 'y' to a coordinate."""

  return (obj["x"], obj["y"])

def getLInfFilter (coord, maxDist):
  """Construct a closure that filters coordinates by L-infinity distance.

  Return a closure that filters coordinates by their L-infinity distance
  to 'coord', with the maximum accepted distance being 'maxDist'.  This
  can be used together with 'filter' to look for nearby hearts or loot,
  for instance."""

  def isInSight (c):
    return distLInf (coord, c) <= maxDist

  return isInSight

################################################################################
# Map

class Map (object):
  """Utility class for map-related tasks.

  Public member variables:
    loot: Dictionary mapping coordinate -> loot amount.
    hearts: List of heart coordinates on the map."""

  def __init__ (self, rpc, state):
    """Create the object, based on a current game state."""

    self._rpc = rpc;
    self.update (state)

  def update (self, state):
    """Update to the new game state."""

    self._state = state

    self.loot = dict ()
    for obj in self._state.state["loot"]:
      self.loot[coordFromJson (obj)] = obj["amount"]

    self.hearts = set ()
    for obj in self._state.state["hearts"]:
      self.hearts.add (coordFromJson (obj))

  def getPath (self, start, targets):
    """Calculate waypoints to go from 'start' to one or multiple targets.

    It returns an array of coordinates (as tuples), though, and not yet
    the actual wp array."""

    if isinstance (targets, tuple):
      targets = [targets]
    if not isinstance (targets, list):
      raise RuntimeError ("expected list of tuple as path targets");

    res = []
    for coord in targets:
      wp = self._rpc.call ("game_getpath", [start, coord])
      pts = unpackWaypoints (wp)
      res.extend (pts)
      start = coord

    return res

  def getDistance (self, start, targets):
    """Calculate the distance (in game paths) from start to target(s)."""

    p = self.getPath (start, targets)
    return pathLen (start, p)

  def isSpawnArea (self, coord):
    """Check whether the coordinate is in a spawn area.

    This is directly based on the code in gamemap.h of the daemon sources."""

    def checkOnce (a, b):
      if b >= SPAWN_LEN and b < MAP_SIZE - SPAWN_LEN:
        return False
      return a in [0, MAP_SIZE - 1]

    return checkOnce (coord[0], coord[1]) or checkOnce (coord[1], coord[0])

  def getSpawnArea (self, colour):
    """Return all spawn coordinates of the given colour."""

    rangeLow = range (0, SPAWN_LEN)
    rangeHigh = range (MAP_SIZE - SPAWN_LEN, MAP_SIZE)

    res = []
    if colour == 0:
      res.extend ([(0, y) for y in rangeLow])
      res.extend ([(x, 0) for x in rangeLow])
    elif colour == 1:
      res.extend ([(MAP_SIZE - 1, y) for y in rangeLow])
      res.extend ([(x, 0) for x in rangeHigh])
    elif colour == 2:
      res.extend ([(MAP_SIZE - 1, y) for y in rangeHigh])
      res.extend ([(x, MAP_SIZE - 1) for x in rangeHigh])
    elif colour == 3:
      res.extend ([(0, y) for y in rangeHigh])
      res.extend ([(x, MAP_SIZE - 1) for x in rangeLow])
    else:
      raise RuntimeError ("invalid colour for Map.getSpawn")
    res = set (res)

    for c in res:
      assert self.isSpawnArea (c)
    assert len (res) == 2 * SPAWN_LEN - 1

    return list (res)
