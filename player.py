#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Handle players and hunters.  This includes creation of new players
# (including name_new / name_firstupdate handling) as well as issuing
# of basic commands.

import json
import logging
import maputils

MIN_NAMENEW_CONF = 2

# Textual representation of the colours.
COLOURS = ["yellow", "red", "green", "blue"]

################################################################################
# HunterMove

class HunterMove(object):
  """Define a movement to be done by a hunter.

  There are three possibilities:  Either move along some path,
  issue a destruct command, or (explicitly) do nothing.  Doing
  nothing is useful, because it can signal that the hunter really
  should leave its state alone and not be moved by some less-priority
  behaviour."""

  def __init__ (self, path = None, destruct = False, wait = False):
    """Initialise the object.

    It must have exactly one of the keyword arguments set.  The path
    must already be fully calculated according to game_getpath."""

    if wait:
      if (not path is None) or destruct:
        raise RuntimeError ("too many actions in HunterMove")
      self._type = "wait"
      return

    assert not wait
    if destruct:
      if not path is None:
        raise RuntimeError ("too many actions in HunterMove")
      self._type = "destruct"
      return
      
    assert not destruct
    if path is None:
      raise RuntimeError ("no action in HunterMove")
    self._type = "move"
    self._path = path

  def addToJson (self, ind, obj):
    """Add the action to the daemon JSON.

    Encapsulate this action as JSON for the daemon.  The
    JSON object 'obj' will be extended, and the data set
    to its 'ind' index.

    It returns True if there's actually a move to make (as opposed
    to a "wait" move)."""

    strInd = str (ind)
    if self._type == "wait":
      return False

    obj[strInd] = {}

    if self._type == "destruct":
      obj[strInd]["destruct"] = True
    else:
      assert self._type == "move"
      obj[strInd]["wp"] = maputils.packWaypoints (self._path)
    return True

################################################################################
# Hunter

class Hunter(object):
  """A hunter.

  Public member variables:
    player: Reference to Player object that holds this hunter.
    ind: Index of this hunter with the player.
    status: String denoting the hunter's status, see below.
    state: JSON object for this hunter according to the game state,
           only if status is 'foreign' or 'active'.

  Hunter states:
    "invalid": This hunter has been killed in the game.
    "pending": The hunter's player has a pending tx, so the hunter is
               currently not available for moves.
    "foreign": This hunter is not controlled by the user.
    "active": This hunter is available for movement commands."""

  def __init__ (self, player, ind, rpc, m):
    """Initialise a hunter object.

    It belongs to the given player with the given index.  Note that this
    routine should not be called externally, but will be done automatically
    by the player object for every hunter it observes."""

    self.player = player
    self.ind = ind
    self._rpc = rpc
    self._map = m

    if self.ind in self.player.hunters:
      raise RuntimeError ("the player already holds this hunter")

    self.player._addHunter (self)
    self.update ()

  def update (self):
    """Update for a new game state.

    We don't need the game state, since it is stored in the player
    object already.  The player needs to be updated before
    the hunters are."""

    # Clear the state and also all queued moves after
    # the state has been updated.
    self.state = None
    self._move = None

    if self.player.status in ["available", "registered",
                              "registering", "activating"]:
      self.status = "invalid"
      return

    assert self.player.status in ["active", "pending", "foreign"]

    strInd = str (self.ind)
    if not strInd in self.player.state:
      self.status = "invalid"
    else:
      self.status = self.player.status
      self.state = self.player.state[strInd]

  def isGeneral (self):
    """Return true iff this is a general."""

    return self.ind == 0

  def getPosition (self):
    """Return the hunter's position."""

    if self.state is None:
      raise RuntimeError ("getPosition on hunter with wrong status")

    return maputils.coordFromJson (self.state)

  def getLoot (self):
    """Return the hunter's total loot hold."""

    if self.state is None:
      raise RuntimeError ("getLoot on hunter with wrong status")

    return self.state["loot"]

  def getValue (self):
    """Return the total value (loot plus general) on this hunter."""

    if self.state is None:
      raise RuntimeError ("getValue on hunter with wrong status")

    res = self.getLoot ()
    if self.isGeneral ():
      res += self.player.state["coinAmount"]

    return res

  def getMovement (self):
    """Return the currently locked-in movement.

    Return the path this hunter is currently moving on according
    to the network.  It is 'None' if the hunter is not moving
    at all."""

    if self.state is None:
      raise RuntimeError ("getMovement on hunter with wrong status")

    if not "wp" in self.state:
      return None

    wp = self.state["wp"]
    return maputils.unpackWaypoints (wp)

  def getTimeToDest (self):
    """Return the time until the hunter reaches the current destination."""

    p = self.getMovement ()
    if p is None:
      return 0

    return maputils.pathLen (self.getPosition (), p)

  def getColour (self):
    """Return the hunter's colour."""

    col = self.player.getColour ()
    assert not col is None
    return col

  def getRemainingLife (self):
    """Return the hunter's remaining life.

    This is a number of blocks if the hunter (actually, player)
    is poisoned.  If the current time is infinite, return None."""

    return self.player.getRemainingLife ()

  def isMoving (self):
    """Return whether or not the hunter is currently moving."""

    return not self.getMovement () is None

  def moveWait (self):
    """Queue a 'wait' move for this hunter."""

    self._queueMove (HunterMove (wait = True))

  def moveDestruct (self):
    """Queue a 'destruct' move for this hunter."""

    self._queueMove (HunterMove (destruct = True))

  def movePath (self, path):
    """Queue movement along a path.

    The path can only be a target, and will be expanded
    according to game_getpath."""

    wp = self._map.getPath (self.getPosition (), path)
    self._queueMove (HunterMove (path = wp))

  def moveClear (self):
    """Unqueue every move."""

    self._queueMove (None)

  def hasMove (self):
    """Query whether we have a queued move or not."""

    return not self._move is None

  def extractMove (self, obj):
    """Extract the queued move to a JSON object.

    Store the queued move (if we have one) to the JSON object.
    It is removed from the hunter afterwards.  Returns True if
    a move was present and False if not."""

    if not self.hasMove ():
      return False

    assert self.status == "active"

    res = self._move.addToJson (self.ind, obj)
    self.moveClear ()
    return res

  def toString (self, verbose = True):
    """Construct a debug string representation of this hunter.

    If verbose is set to False, only the hunter name will
    be included.  Otherwise, also some status information."""

    name = "%s.%d" % (self.player.name, self.ind)
    if not verbose:
      return name

    state = self.status
    if not self.state is None:
      if self.isMoving ():
        state += ", moving"
      state += ", %.8f HUC" % self.getLoot ()
    return "%s: %s" % (name, state)

  def _queueMove (self, move):
    """Internal routine to queue a move.

    This fails if the status is wrong."""

    if self.status != "active":
      raise RuntimeError ("cannot move hunter with wrong status")

    self._move = move

################################################################################
# Player

class Player(object):
  """A player.

  Public member variables:
    name: The name of this player as a string.
    hunters: Array of child Hunter objects.
    status: String denoting the player status, see below.
    state: JSON object corresponding to this player according to
           the game state.  Only if status is 'foreign' or 'active'.

  Player states:
    "pending": This player has a pending tx, but exists on the
               map and is controlled by the user.
    "available": This player doesn't exist and the name is available for
                 registration.  This means that a name_new could be issued
                 by calling the register() method.  Usually it means that
                 the player has been killed.
    "registering": name_new is waiting for sufficient confirmations.
    "registered": name_new is confirmed, and activate() can be called
                  to issue name_firstupdate.
    "activating": name_firstupdate has been issued but is not yet confirmed.
    "active": The player is alive and available for updates.
    "foreign": The player is alive but not controlled by the user."""

  def __init__ (self, name, rpc, m, state):
    """Initialise a player object.

    This sets the object to use the given RPC interface, and
    initialises the state according to the game state."""

    self.name = name
    self.hunters = {}
    self._rpc = rpc
    self._map = m

    self._tx = None
    self._rand = None
    self._pendingFirstupdate = False
    self.update (state)

  def update (self, state):
    """Update to the new game state.

    Update the internal data to the game state.  In particular, this
    checks the state of this player.  It also updates the list of hunters
    to match precisely what it is in the game state."""

    # Check player state itself.
    self._internalUpdate (state)

    # Construct hunter objects that aren't yet present.
    stateHunters = 0
    if not self.state is None:
      for (key, val) in self.state.iteritems ():
        try:
          ind = int (key)
          stateHunters += 1
          if not ind in self.hunters:
            h = Hunter (self, ind, self._rpc, self._map)
            assert h is self.hunters[ind]
        except ValueError:
          pass

    # Update all hunter objects we have.
    for (i, h) in self.hunters.iteritems ():
      h.update ()

    # Remove all hunter objects that are invalid.
    newHunters = {}
    for (i, h) in self.hunters.iteritems ():
      if h.status != "invalid":
        newHunters[i] = h
    self.hunters = newHunters

    # Check for consistency.
    assert len (self.hunters) == stateHunters

  def getColour (self):
    """Return the player's colour (as integer 0-3).

    For 'activating' players, return the colour to which it has
    been firstupdated.  Otherwise, return None if there's no associated
    player state."""

    if self.status == "activating":
      return self._firstupdateColour

    if self.state is None:
      return None

    return self.state["color"]

  def getRemainingLife (self):
    """Return the player's remaining life.

    This is a number of blocks if the player
    is poisoned.  If the current time is infinite, return None."""

    if self.state is None:
      raise RuntimeError ("getRemainingLife on player with wrong status")

    if "poison" in self.state:
      return self.state["poison"]

    return None

  def register (self):
    """Start the registration of an available player.

    This issues the name_new command."""

    if self.status != "available":
      logging.error ("Player '%s' is %s and cannot be registered.",
                     self.name, self.status)
      raise RuntimeError ("wrong player status for 'register'")
    assert (self._tx is None) and (self._rand is None)
    assert not self._pendingFirstupdate

    res = self._rpc.call ("name_new", [self.name])
    self._tx = res[0]
    self._rand = res[1]
    self.status = "registering"
    logging.info ("name_new '%s': %s, %s", self.name, self._tx, self._rand)

  def activate (self, colour):
    """Activate (name_firstupdate) a 'registered' player.

    The desired colour must be passed, as integer 0-3."""

    if self.status != "registered":
      logging.error ("Player '%s' is %s and cannot be activated.",
                     self.name, self.status)
      raise RuntimeError ("wrong player status for 'activate'")
    assert (self._tx is not None) and (self._rand is not None)
    assert not self._pendingFirstupdate

    val = json.dumps (dict (color=colour))
    args = [self.name, self._rand, self._tx, val]
    res = self._rpc.call ("name_firstupdate", args)
    self._tx = res
    self._rand = None
    self._pendingFirstupdate = True
    self._firstupdateColour = colour
    self.status = "activating"
    logging.info ("name_firstupdate '%s': %s", self.name, self._tx)

  def sendMoves (self):
    """Send all queued moves of hunters to the network.

    Returns True if moves have been sent, False otherwise."""

    if self.status != "active":
      logging.error ("Player '%s' is %s and cannot be moved.",
                     self.name, self.status)
      raise RuntimeError ("wrong player status for movement")

    hasMoves = False
    obj = {}
    for (i, h) in self.hunters.iteritems ():
      if h.extractMove (obj):
        hasMoves = True

    if hasMoves:
      val = json.dumps (obj)
      res = self._rpc.call ("name_update", [self.name, val])
      self._tx = res
      self.status = "pending"
      logging.info ("name_update '%s': %s", self.name, self._tx)

    return hasMoves

  def toString (self, withHunters = False):
    """Build a debug string representing information about the player."""

    res = "Player %s: %s" % (self.name, self.status)
    if not self.state is None:
      res += ", %s" % COLOURS[self.getColour ()]
    if withHunters:
      for (i, h) in self.hunters.iteritems ():
        res += "\n  " + h.toString ()

    return res

  def _internalUpdate (self, state):
    """Update to the new game state.

    This is an internal routine that does not update the hunters."""

    self.state = None

    # Check if the name has been taken away by someone else.
    # If it has, reset a possible pending transaction.
    if (not self.name in state.names) and state.nameExists (self.name):
      self.status = "foreign"
      self._tx = None
      self._rand = None
      self._pendingFirstupdate = False
      self.state = state.state["players"][self.name]
      return

    # If we have a txid, query for its confirmation status.
    if self._tx is not None:
      data = self._rpc.call ("gettransaction", [self._tx])
      confirmations = data["confirmations"]        

    # If we have a rand value, we are in registration phase.
    # Handle this here right now.
    if self._rand is not None:
      assert not self.name in state.names
      assert not state.nameExists (self.name)
      assert not self._pendingFirstupdate
      assert self._tx is not None
      if confirmations < MIN_NAMENEW_CONF:
        self.status = "registering"
      else:
        self.status = "registered"
      return

    # Bail early if we have a pending firstupdate.
    if self._pendingFirstupdate:
      assert self._tx is not None
      if confirmations == 0:
        self.status = "activating"
        return
      self._pendingFirstupdate = False
      del self._firstupdateColour

    # If the player has been killed, remove it.
    assert not self._pendingFirstupdate
    assert self._rand is None
    if not self.name in state.names:
      assert not state.nameExists (self.name)
      self.status = "available"
      self._tx = None
      return

    # Set the state.
    assert (self.name in state.names) and state.nameExists (self.name)
    self.state = state.state["players"][self.name]

    # Check for pending transaction.
    if self._tx is not None:
      if confirmations == 0:
        self.status = "pending"
        return
      self._tx = None

    # This should be an active player name.
    assert self._tx is None
    self.status = "active"

  def _addHunter (self, h):
    """Add a hunter object to be governed by this player."""

    assert self is h.player
    assert not h.ind in self.hunters
    self.hunters[h.ind] = h
