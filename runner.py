#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Implement a basic strategy that can be used to "run" for the crown
# and coins dropped right after a disaster.  This can be seen
# as an example for a strategy different from the SoulMaster-based model.

import behaviours
import controller
import maputils
import logging
import player
import souls

MAX_DISASTER_TIME = 1
CROWN_ENEMY_DISTANCE = 10
CROWN_REMAINING_LIFE = 10

################################################################################
# DisasterGatherer

class DisasterGatherer (souls.Behaviour):
  """Behaviour for a gatherer during disaster running.

  This behaviour collects coins according to Gather until
  no more coins are available, then moves back to cash in.  After
  this first step, it follows the Gather/CashIn strategy instead."""

  def __init__ (self, gather, cashIn):
    """Initialise with the given basic Gather / CashIn behaviours."""

    self._gather = gather
    self._cashIn = cashIn
    self._initialRun = True

    # We want no forced wait initially, so that we
    # can find out about 'no more coins visible'.
    self._gather.setForcedWait (False)

  def play (self, hunter, control):
    """Implement the play() strategy."""

    # If we are not in the initial run, use the CashIn strategy.
    if not self._initialRun:
      self._cashIn.play (hunter, control)
      if hunter.hasMove ():
        return

    # Use the Gather strategy.
    self._gather.play (hunter, control)

    # If we have no more coins, run cashIn once more and
    # reset the status.
    if self._initialRun and (not hunter.hasMove ()):
      self._gather.setForcedWait (True)
      self._initialRun = False
      self._cashIn.play (hunter, control)

################################################################################
# CrownHunter

class CrownHunter (souls.Behaviour):
  """Behaviour for hunting the crown during a disaster run.

  This behaviour moves the hunter to the crown if it is on the ground.
  If the crown is taken by someone else, it simply does nothing.  If the
  hunter becomes the lucky crown holder, it tries to move back to spawn and
  wait there.  If some enemy moves too close, it will bank to secure
  the collected coins."""

  def play (self, hunter, control):
    """Implement the play() strategy."""

    # If the crown is on the ground, move to it
    # (unless we already move there).
    crown = control.getCrown ()
    if isinstance (crown, tuple):
      if behaviours.alreadyMovingTowards (hunter, crown):
        logging.info ("CrownHunter %s: Already moving towards crown.",
                      hunter.toString (False))
        hunter.moveWait ()
      else:
        logging.info ("CrownHunter %s: Moving to crown at %s.",
                      hunter.toString (False), str (crown))
        hunter.movePath (crown)
      return
    assert isinstance (crown, player.Hunter)

    # If someone else is the crown holder, do nothing.
    if crown is not hunter:
      return

    # I am the crown holder.  Go hiding if we're not yet there.
    assert crown is hunter
    colour = hunter.getColour ()
    pos = hunter.getPosition ()
    hiding = [(1, 1), (500, 1), (500, 500), (1, 500)]
    if pos != hiding[colour]:
      if behaviours.alreadyMovingTowards (hunter, hiding[colour]):
        logging.info ("CrownHunter %s: Already moving to hiding place.",
                      hunter.toString (False))
        hunter.moveWait ()
      else:
        logging.info ("CrownHunter %s: Moving to hiding place at %s.",
                      hunter.toString (False), str (hiding[colour]))
        hunter.movePath (hiding[colour])
      return

    # We are hiding.  Check for enemies or poisoning and bank
    # if it seems necessary.

    assert pos == hiding[colour]
    shouldBank = False

    life = hunter.getRemainingLife ()
    if (life is not None) and life < CROWN_REMAINING_LIFE:
      logging.info ("CrownHunter %s: Remaining life %d, banking.",
                    hunter.toString (False), life)
      shouldBank = True
    else:
      for h in control.hunters["foreign"]:
        if h.getColour () != colour:
          if maputils.distLInf (h.getPosition (), pos) < CROWN_ENEMY_DISTANCE:
            logging.info ("CrownHunter %s: Enemy %s too close, banking.",
                          hunter.toString (False), h.toString (False))
            shouldBank = True
            break

    banking = [(0, 0), (501, 0), (501, 501), (0, 501)]
    if shouldBank:
      assert maputils.distLInf (pos, banking[colour]) == 1
      hunter.movePath (banking[colour])
    else:
      logging.info ("CrownHunter %s: Hiding.", hunter.toString (False))
      hunter.moveWait ()

################################################################################
# DisasterRunner

class DisasterRunner (controller.Strategy):
  """Game strategy to run for loot after a disaster.

  This strategy keeps a certain amount of players in reserve (so that
  they can be immediately activated) and starts them to run for the
  crown and loot as soon as a disaster happens.

  Gathering players will also be launched to certain coin areas.  They
  will collect there until no more coins are visible and then return to cash in.
  This is probably not really an optimal behaviour to collect coins after a
  disaster, but we'll do it anyway for simplicity.  Afterwards, they will
  behave like ordinary Gather/CashIn bots.

  If the crown gets dropped (this is probably not the case already
  before the disaster strikes), try to dispatch the hunter nearest to
  it towards it.  Presumably, the crown holder will bank when disaster
  strikes, so that the crown becomes available soon afterwards.  At
  this point in time, the running crowd won't yet be out of the
  "gate" of the spawn area, so that we can aim straight for the crown
  optimally if we have one hunter of each colour."""

  def __init__ (self, gatherers):
    """Initialise the strategy with given gathering configuration.

    'gatherers' is a list of parameters for gatherers to create, where each
    entry is a dict with these fields:
      "colour": Colour of the gatherer.
      "gather": Gatherer behaviour to use for collecting.
      "cashIn": CashIn behaviour to use after the initial collection."""

    self._gatherers = gatherers
    self._status = "initialised"

    self._crownBehaviour = CrownHunter ()
    def createBehaviour (g):
      return DisasterGatherer (g["gather"], g["cashIn"])
    self._behaviours = map (createBehaviour, self._gatherers)

  def play (self, control):
    """Perform the strategy."""

    # Now that we have the Controller, actually
    # start player registration in case we have
    # not yet done so.
    if self._status == "initialised":
      control.registerPlayers (len (self._gatherers))
      self._status = "preparing"
      return True

    # If we are in "preparing" status, check if the name_new transactions
    # have already confirmed accordingly and update the status.
    if self._status == "preparing":
      if control.playersInReserve () >= len (self._gatherers):
        self._status = "waiting"
        self._lastDisasterTime = control.state.timeSinceDisaster ()
      return True

    # In the "waiting" status, see if a disaster has happened
    # recently and activate the players if that is the case.
    if self._status == "waiting":
      time = control.state.timeSinceDisaster ()
      if time > self._lastDisasterTime or time > MAX_DISASTER_TIME:
        logging.info ("Disaster runner is ready, waiting for next disaster.")
        return True

      self._status = "running"
      logging.info ("Disaster happened %d blocks ago, running!", time)
      self._crownHunter = None
      self._players = []
      self._hunters = []
      for g in self._gatherers:
        p = control.activatePlayer (g["colour"])
        assert p is not None
        self._players.append (p)
        self._hunters.append (None)
      return True

    # We're already running.  For players that have finished registration
    # but where we don't yet have a hunter on file, fix this.  Always
    # use the general for simplicity.
    assert self._status == "running"
    haveAllHunters = True
    for i in range (len (self._gatherers)):
      if self._hunters[i] is None:
        haveAllHunters = False
        if self._players[i].status == "active":
          self._hunters[i] = self._players[i].hunters[0]

    # If we don't have a crown hunter but the crown is on the ground,
    # move the closest player to it.  Do this only once all hunters
    # have been active (so that we find indeed the closest one).  There
    # should be time enough for them to activate until we reach
    # the spawn gate anyway.
    if self._crownHunter is None:
      crown = control.getCrown ()
      if isinstance (crown, tuple) and haveAllHunters:
        bestHunter = None
        for h in self._hunters:
          d = control.map.getDistance (h.getPosition (), crown)
          if bestHunter is None or d < bestDist:
            bestHunter = h
            bestDist = d
        self._crownHunter = bestHunter

    # Perform crown hunter behaviour.  If the behaviour doesn't
    # issue any move at all, revert to default gathering behaviour.
    # This is not an "else" to the previous conditional because
    # it should be run when we newly set the crownHunter there.
    if self._crownHunter is not None:
      if self._crownHunter.status == "active":
        self._crownBehaviour.play (self._crownHunter, control)
        if not self._crownHunter.hasMove ():
          self._crownHunter = None

    # Perform all other behaviours.  Keep track of the situation that
    # all hunters are defined but invalid (killed) and exist the
    # strategy if that is the case.
    allInvalid = True
    for i in range (len (self._gatherers)):
      h = self._hunters[i]
      if (h is None) or h.status != "invalid":
        allInvalid = False
      if (h is None) or (h is self._crownHunter):
        continue
      if h.status == "active":
        self._behaviours[i].play (h, control)

    if allInvalid:
      logging.info ("All hunters have been killed, exiting.")
    return not allInvalid
