#!/usr/bin/python

#   AutoHunter, a botting framework for Huntercoin
#   Copyright (C) 2014 Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Souls are immortal, unless they decide for the nirvana.
# This module contains everything around the "SoulMaster" strategy,
# which abstracts behaviours away from actual hunters and makes
# hunter recreation transparent in this way.

import collections
import controller
import logging
import player

################################################################################
# Soul

class Soul (object):
  """An abstracted behaviour for a single hunter.

  'Souls' are the concept of AutoHunter that makes recreation of
  hunters transparent:  A Soul defines a behaviour for a single hunter,
  but it is not bound to a particular one.  If the one tied to it dies,
  it may choose another one from the pool of available hunters.

  This is the abstract base class that will be subclassed to define
  actual behaviours (including recreation behaviour).

  Public member variables:
    hunter: The controlled hunter.  Might be None.
    name: Name of this Soul for debugging purposes."""
  
  def __init__ (self, name = "<unknown soul>"):
    """Initialise.

    Set the owned hunter to None for now.  The optional name
    will be used for debug logging."""

    self.hunter = None
    self.name = name
    self._initialised = False

  def getHunter (self):
    """Return the owned hunter or None.

    This takes care of the case of a killed hunter.  If the hunter's status
    is 'invalid', set it to None."""

    if self.hunter is None:
      return None

    if self.hunter.status == "invalid":
      self.hunter = None
      return None

    # 'foreign' should not be possible, since the soul should only
    # be connected to one of our hunters in the first place.
    assert self.hunter.status in ["pending", "active"]

    return self.hunter

  def isInitialised (self):
    """Return True if this Soul already owned a hunter.

    This routine can be used to check whether this soul ever controlled
    a hunter.  It is True after choosing one for the first time.  This
    information can be used to decide whether to choose one if we don't
    want to recreate hunters for it."""

    return self._initialised

  def setHunter (self, h):
    """Set the owned hunter."""

    assert self.hunter is None
    assert h is not None
    self.hunter = h
    self._initialised = True

    logging.info ("Soul %s now controls %s.",
                  self.name, self.hunter.toString (False))

  def chooseHunter (self, hunters, control):
    """Choose a new hunter to own.

    This routine is called if the Soul doesn't have a hunter,
    and it must choose one from the 'hunters' list and return it.
    If there is no hunter available that suits it, it can return
    an integer 0-3 instead, which means that it wants a new
    hunter of that colour.  The 'control' instance can be used
    to find out which colours are already in the registration process,
    if that is desired.

    If the soul shouldn't be connected for a hunter for now,
    it can return True/False.  True means that it wants to stay
    "active" and be asked again in the future, and False means
    that the soul will be removed from the SoulMaster."""

    raise NotImplementedError ("Soul.chooseHunter not overridden")

  def play (self, control):
    """Issue appropriate commands to the hunter.

    This is the main routine.  It is called if we have an 'active' hunter
    associated, and should issue the appropriate commands given the
    Controller to access the game state, map and everything.
    
    It should return True if everything is fine, and False if the Soul
    should release the hunter."""

    raise NotImplementedError ("Soul.play not overridden")

  def _internalPlay (self, control):
    """Internal routine that 'dresses' the play routine a bit.

    This is the routine actually called by the SoulMaster, and it wraps
    around the 'play' routine provided by the implementor to do some
    additional processing."""

    assert self.getHunter () is not None
    if self.hunter.status == "pending":
      return

    assert self.hunter.status == "active"
    res = self.play (control)

    if not res:
      self.hunter = None

################################################################################
# ChooseByColour

class ChooseByColour (object):
  """Implement a hunter choosing strategy based only on desired colour.

  This class implements the chooseHunter() method for Soul based only
  on one or more desired colours.  It can be subclassed via multiple
  inheritance by a Soul implementation to take care of the chooseHunter
  routine.

  If multiple hunters are readily available, it prefers non-generals."""

  def __init__ (self, colours, recreate = True):
    """Construct the object with the given configuration.

    The strategy tries to find an existing hunter with one of the
    colours in the given array, and if it can't find one, requests
    a hunter with colour given by the first entry in colours.

    If 'recreate' is False, it won't choose a hunter after having one
    for the first time.  It will completely remove itself from the game,
    since it doesn't make sense to keep it around as it won't pick
    any more anyway."""

    if not isinstance (colours, list):
      colours = [colours]
    if not isinstance (colours, list) or len (colours) == 0:
      raise RuntimeError ("invalid 'colours' given for ChooseByColour")

    self._colours = colours
    self._recreate = recreate

  def chooseHunter (self, hunters, control):
    """Implement the chooseHunter logic."""

    if (not self._recreate) and self.isInitialised ():
      return False

    foundGeneral = None
    for h in hunters:
      if h.getColour () in self._colours:
        if h.isGeneral ():
          if foundGeneral is None:
            foundGeneral = h
        else:
          return h

    if foundGeneral is not None:
      return foundGeneral

    return self._colours[0]

################################################################################
# Behaviour

class Behaviour (object):
  """Abstract class defining a hunter's "behaviour".

  This class defines just a logic for play() (slightly modified,
  since the return value is ignored).  These behaviours can be built
  together with an appropriate chooseHunter() logic and the
  StackedBehaviour object."""

  def play (self, hunter, control):
    """Perform the action.

    Perform the logic on 'hunter'.  If no move (not even moveWait()) is
    issued to it, this means that the behaviour sees no necessary
    action to take.  No return value is necessary."""

    raise NotImplementedError ("Behaviour.play not overridden")

################################################################################
# StackedBehaviours

class StackedBehaviours (object):
  """Define 'play' by working through a list of stacked behaviours.

  This class implements the play() method by providing a list
  of stacked Behaviours.  This list is processed in order, and
  each behaviour gets the chance to perform some action.  If it
  does some move (also moveWait() is possible) with the controlled
  hunter, the others won't be tried anymore.

  Note that it assumes that self.hunter is set to the controlled
  hunter (as is the case for a Soul)."""

  def __init__ (self, behaviours = []):
    """Initialise with the list of behaviours."""

    self._behaviours = collections.deque (behaviours)

  def addHighPrio (self, b):
    """Add 'b' to the high-priority side of the list."""

    self._behaviours.appendleft (b)

  def addLowPrio (self, b):
    """Add 'b' to the low-priority side of the list."""

    self._behaviours.append (b)

  def play (self, control):
    """Implement the play() routine."""

    for b in self._behaviours:
      b.play (self.hunter, control)
      if self.hunter.hasMove ():
        break

    return True

################################################################################
# SoulMaster

class SoulMaster (controller.Strategy):
  """Strategy defined by a collection of Souls.

  This class implements a controller strategy that is defined by
  a collection of Souls.  They can voice their preferences for
  hunters via 'chooseHunter', and the SoulMaster will manage the
  available hunters to fulfill the wishes."""

  def __init__ (self):
    """Construct the SoulMaster.

    At the beginning, it won't hold any souls."""

    controller.Strategy.__init__ (self)
    self._souls = []

  def addSoul (self, s):
    """Add the given soul."""

    self._souls.append (s)

  def play (self, control):
    """Implement the Strategy.play method."""

    # As a first step, get a list of hunters hold by souls.
    ownedHunters = set ()
    freeSouls = 0
    for s in self._souls:
      h = s.getHunter ()
      if h is not None:
        ownedHunters.add (h)
      else:
        freeSouls += 1
    logging.info ("SoulMaster has %d souls, %d are free.",
                  len (self._souls), freeSouls)

    # Now build a list of available hunters.
    available = set ()
    for h in control.hunters["mine"]:
      if h not in ownedHunters:
        available.add (h)

    # Ask every free soul to pick a hunter.  This builds up
    # a new list of souls, so that we can decide not to add
    # those who return False.  It also builds up the counts of
    # colours that should be newly registered.
    # Register colours will be just a simple list of colours
    # to register, so that they can be activated in the same list
    # (priority) as the souls.
    registerColours = []
    newSouls = []
    removedSouls = 0
    for s in self._souls:
      if s.getHunter () is None:
        res = s.chooseHunter (available, control)
        if isinstance (res, player.Hunter):
          available.remove (res)
          s.setHunter (res)
          newSouls.append (s)
        elif isinstance (res, bool):
          if res:
            newSouls.append (s)
          else:
            removedSouls += 1
        elif isinstance (res, int):
          registerColours.append (res)
          newSouls.append (s)
        else:
          raise RuntimeError ("Soul.chooseHunter returned invalid type")
      else:
        newSouls.append (s)
    assert (len (newSouls) + removedSouls == len (self._souls))
    self._souls = newSouls

    # For souls that have a hunter, perform the play operation.
    for s in self._souls:
      if s.getHunter () is not None:
        s._internalPlay (control)

    # Handle registration of the desired hunters.  They can not yet
    # be associated to souls, of course, but will be available to be
    # picked in the next steps.
    registerCount = 0
    inActivation = control.playersInActivation ("array")
    for c in registerColours:
      if inActivation[c] > 0:
        # Note:  We assume that one player = one hunter.
        # This is true since the poison-hardfork, and shouldn't
        # be too bad an assumption in general.  The worst
        # that can happen is that we register additional hunters,
        # which will be available in the reserve pool and used
        # later anyway.
        inActivation[c] -= 1
      elif control.playersInReserve () > 0:
        # Activate a player.  Note that this ensures that
        # control.playersInReserve() returns less the next
        # time.  We also know that there aren't any more
        # players in activation of that colour, so that
        # it doesn't screw up with the clause above (we
        # have a copy that won't automagically update).
        assert inActivation[c] == 0
        control.activatePlayer (c)
      else:
        assert inActivation[c] == 0 and control.playersInReserve () == 0
        registerCount += 1
    registerCount -= control.playersInRegistration ()
    if registerCount > 0:
      control.registerPlayers (registerCount)

    # TODO: Maybe return False here if we don't have any active
    # souls any longer.  But only when the Controller handles graceful
    # exits without losing player registration info.

    return True
